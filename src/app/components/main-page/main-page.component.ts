import { FireBaseApiService } from './../../services/fireBaseApi.service';
import { ToBuyItemModel } from './../../models/to-buy-item-model';
import { Observable, from } from 'rxjs';
import { filter, map, retry } from 'rxjs/operators';

import { Component } from '@angular/core';


@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html'
})
export class MainPageComponent {
  Title1 = 'Main page module';

  constructor(private fireBaseApiService: FireBaseApiService) {
    this.fireBaseApiService.initDB();
  }

  get tobuyList() {
    const toBuyList: ToBuyItemModel[] = this.fireBaseApiService.getDBdata();
    return toBuyList;
  }

  get tobuyListObservableChecked() {
    const tobuyListItems: ToBuyItemModel[] = [];

    this.fireBaseApiService.getDBdataObservable()
    .pipe(filter(item => item.checked === true))
    .subscribe(item => tobuyListItems.push(item));
    return tobuyListItems;
  }

  add() {
    const toBuyItem: ToBuyItemModel = {
      id: Math.random(),
      title: 'newItem -' + (+(new Date())),
      amount: Math.round(Math.random() * 100),
      checked: false
    };

    this.fireBaseApiService.add(toBuyItem);
  }

  update(item: ToBuyItemModel) {
    item.title = 'updatedItem -' + (+(new Date()));
    item.checked = !item.checked;

    this.fireBaseApiService.update(item.id, item);
  }

  remove(item: ToBuyItemModel) {
    this.fireBaseApiService.remove(item.id);
  }
}
