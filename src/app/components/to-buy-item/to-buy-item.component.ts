import { ToBuyItemModel } from './../../models/to-buy-item-model';
import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-to-buy-item',
  templateUrl: './to-buy-item.component.html'
})
export class ToBuyItemComponent {

  @Input() item: ToBuyItemModel;
  @Output() updateItem: EventEmitter<ToBuyItemModel> = new EventEmitter();
  @Output() deleteItem: EventEmitter<ToBuyItemModel> = new EventEmitter();

  update(item: ToBuyItemModel) {
    this.updateItem.emit(item);
  }

  delete(item: ToBuyItemModel) {
    this.deleteItem.emit(item);
  }
}
