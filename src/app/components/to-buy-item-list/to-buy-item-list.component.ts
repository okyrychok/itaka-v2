import { ToBuyItemModel } from './../../models/to-buy-item-model';
import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-to-buy-item-list',
  templateUrl: './to-buy-item-list.component.html'
})
export class ToBuyItemListComponent {

  @Input() itemList: Array<ToBuyItemModel>;
  @Output() updateItemList: EventEmitter<ToBuyItemModel> = new EventEmitter();
  @Output() deleteItemList: EventEmitter<ToBuyItemModel> = new EventEmitter();

  update(item: ToBuyItemModel) {
    this.updateItemList.emit(item);
  }

  delete(item: ToBuyItemModel) {
    this.deleteItemList.emit(item);
  }
}
