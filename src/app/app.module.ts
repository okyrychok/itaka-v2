import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import {FireBaseApiService} from './services/fireBaseApi.service';

import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [FireBaseApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
