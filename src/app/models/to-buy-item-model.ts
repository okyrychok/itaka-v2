export interface ToBuyItemModel {

      id: number;
      title: string;
      amount: number;
      checked: boolean;
}
