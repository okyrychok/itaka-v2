import { ToBuyItemModel } from './to-buy-item-model';

export interface ToBuyItemListModel {
  ToBuyItems: ToBuyItemModel[];
}
