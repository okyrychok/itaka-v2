import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';

import {FireBaseApiService} from '../services/fireBaseApi.service';

import { ToBuyItemComponent } from '../components/to-buy-item/to-buy-item.component';
import { ToBuyItemListComponent } from '../components/to-buy-item-list/to-buy-item-list.component';
import { MainPageComponent } from '../components/main-page/main-page.component';

@NgModule({
  imports: [
    CommonModule,
    BrowserModule
  ],
  declarations: [
    MainPageComponent,
    ToBuyItemComponent,
    ToBuyItemListComponent
  ],
  bootstrap: [MainPageComponent],
  providers: [FireBaseApiService]
})
export class MainPageModule { }
