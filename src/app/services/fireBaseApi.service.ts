import {Injectable} from '@angular/core';
import { Observable, from } from 'rxjs';
import { ToBuyItemModel } from '../models/to-buy-item-model';

declare var firebase: any;

@Injectable()
export class FireBaseApiService {
  config: any = {
    apiKey: 'AIzaSyAuNm5CIHCsUsV3ENTtMnxFNElIiRxbM8k',
    authDomain: 'angular-course-b363c.firebaseapp.com',
    databaseURL: 'https://angular-course-b363c.firebaseio.com',
    projectId: 'angular-course-b363c',
    storageBucket: 'angular-course-b363c.appspot.com',
    messagingSenderId: '362166160084'
  };
  database: any;
  DBdata: Array<any> = [];
  DBdataObservable: Observable<ToBuyItemModel> = new Observable<ToBuyItemModel>();

  constructor() {}

  initDB() {
    // Initialize Firebase
     firebase.initializeApp(this.config);

     !firebase.apps.length ? firebase.initializeApp(this.config).firestore()
        : firebase.app().firestore();

    firebase.auth().signInAnonymously()
      .catch(function(error) {
        // Handle Errors here.
        const errorCode = error.code;
        const errorMessage = error.message;

        console.log(`Auth error. errorCode - ${errorCode}, errorMessage - ${errorMessage}`);
      });

    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        this.database = firebase.database();
        this.loadData();
      }
    });
  }

  loadData() {
    const tobuys = firebase.database().ref('tobuy_okyrychok/');

    tobuys.on('value', (tobuyList) => {
      this.DBdata = [];

      tobuyList.forEach(tobuy => {
        this.DBdata.push(tobuy.val());
      });

      this.DBdataObservable = from(this.DBdata);
    });

  }

  getDBdata() {
    return this.DBdata;
  }

  getDBdataObservable() {
    return this.DBdataObservable;
  }

  add(item) {
    const newId = +(new Date());

    item.id = newId;
    firebase.database().ref('tobuy_okyrychok/' + newId).set(item);
  }

  update(id, item) {
    firebase.database().ref('tobuy_okyrychok/' + id).update(item);
  }

  remove(id) {
    firebase.database().ref('tobuy_okyrychok/' + id).remove();
  }
}
